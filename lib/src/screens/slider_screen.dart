import 'package:flutter/material.dart';

class SliderScreen extends StatefulWidget {
  const SliderScreen({super.key});

  @override
  State<SliderScreen> createState() => _SliderScreenState();
}

class _SliderScreenState extends State<SliderScreen> {

  double _sliderValue = 10.0;
  bool _isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Sliders")
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 30.0),
        child: Column(
          children: [
            _createSlider(),
            _createCheckbox(),
            _createSwitch(),
            Text("Tamaño de la imagen: $_sliderValue"),
            Expanded(
              flex: 0,
              child: _createImage()
            )
          ],
        ),
      ),
    );
  }
  
  Widget _createSlider() {
    return Slider(
      min: 10.0,
      max: 400.0,
      value: _sliderValue,
      // * Todos los manejadores de eventos
      // * si reciven null bloquean el elemento UI
      onChanged: _isChecked ? null : (value) {
        setState(() {
          _sliderValue = value;
        });
      },
    );
  }
  
  Widget _createImage() {
    return Image(
      image: const NetworkImage('https://cdn.iconscout.com/icon/free/png-256/free-flutter-3629369-3032362.png'),
      fit: BoxFit.contain,
      height: _sliderValue,
    );
  }
  
  Widget _createCheckbox() {
    return CheckboxListTile(
      title: const Text("Bloquear Slider"),
      subtitle: Text( (_isChecked) ? "Bloqueado" : "Desbloqueado"),
      secondary: const Icon(Icons.photo_size_select_large),
      value: _isChecked, 
      onChanged: (value) {
         setState(() {
           _isChecked = value!;
         });
       }
    );
  }
  
  Widget _createSwitch() {
    return SwitchListTile(
      title: const Text("Bloquear Slider"),
      subtitle: Text( (_isChecked) ? "Bloqueado" : "Desbloqueado"),
      secondary: const Icon(Icons.switch_camera_outlined),
      value: _isChecked, 
      onChanged: (value) {
         setState(() {
           _isChecked = value;
         });
       }
    );
  }
}