import 'package:flutter/material.dart';

import 'dart:async';


class LstScreen extends StatefulWidget {
  const LstScreen({super.key});

  @override
  State<LstScreen> createState() => _LstScreenState();
}

class _LstScreenState extends State<LstScreen> {

  //? Espia del scroll
  final ScrollController _scrollController = ScrollController();

  // Lista que almacena los ids de las imagenes a mostrar 
  List<int> _lstNums = [];

  // Centinela del último id de la lista
  int _lastItem = 0;

  // Bandera de carga
  bool _isLoading = false;

  // * Inicializa el estado
  // * Primer metodo del ciclo de vida de los widgets
  @override
  void initState() {

    // * Inicializa el estado 
    super.initState();

    // * Agrega las imagenes siguientes
    _addNextFive();

    // * Agrregamos un listener al scroll controller
    // * Se crea un nuevo listener cada que se crea este Widget
    // * Se tiene que eliminar cada que se destruya este Widget
    _scrollController.addListener(() {

      // Cunado nos encontramos al final de la pantalla se agregan otras 5 imagenes  
      if( _scrollController.position.pixels == _scrollController.position.maxScrollExtent ) {
        _fetchData();
      }

     });
  }

  // * Metodo que se ejecuta cuando se destruye este Widget
  // * Ciclo de vida
  @override
  void dispose() {
    super.dispose();
    // * Eliminacion de listener
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Listas"),
      ),

      // ? El widget Stack apila Windgets uno sobre otro
      body: Stack(
        children: [
          _createList(),
          _createLoading()
        ],
      )
    );
  }

  Widget _createList(){

    // ? Un builder se usa para renderizor un número indeterminado de elementos (APIs)
    return RefreshIndicator(
      onRefresh: _onlyNextFive,
      child: ListView.builder(
      
        // * Se asigna el Scroll Controller
        controller: _scrollController,
        itemCount: _lstNums.length,
        itemBuilder: (context, index) {
          final int image = _lstNums[index];
          return FadeInImage(
              placeholder: const AssetImage("assets/jar-loading.gif"), 
              image: NetworkImage("https://picsum.photos/id/$image/500/300")
          );
        }
      ),
    );
  }

  void _addNextFive() {
    for (var i = 1; i <= 5; i++) {
      _lastItem++;
      _lstNums.add(_lastItem);
    }
    setState(() {});
  }
  
  Future _fetchData() async {
    _isLoading = true;
    setState(() {});

    // * Simulacion de tiempo de respueta de una API
    return Timer(
      const Duration(seconds: 5),
      httpRes,
    );
    
  }

  void httpRes() {
    _isLoading = false;
    _addNextFive();

    // * Scrolleando abajo cuando ya hay mas elementos
    _scrollController.animateTo(
      _scrollController.position.pixels + 50, 
      duration: const Duration(seconds: 1), 
      curve: Curves.easeInBack
    );
  }
  
  Widget _createLoading() {

    if( _isLoading ) {

      return const Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator()
            ]
          ),
          SizedBox(height: 30.0)
        ],
      );

    }
    else {

      return Container();

    }

  }

  Future _onlyNextFive() async {
    Timer( 
      const Duration(seconds: 2), 
      () {
        _lstNums.clear();
        _lastItem++;
        _addNextFive();
      });
      return Future.delayed( const Duration(seconds: 2) );
  }
}
