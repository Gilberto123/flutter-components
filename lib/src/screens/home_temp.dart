import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  const HomePageTemp({super.key});

  @override
  Widget build(BuildContext context) {
    final List<String> items = ['Uno', 'Dos', 'Tres', 'Cuatro', 'Cinco'];

    /* List<Widget> createListItems() {
      List<Widget> listItems = [];
      for (var itm in items) {
        final ListTile tile = ListTile(title: Text(itm));
        listItems
          ..add(tile)
          ..add(const Divider());
      }
      return listItems;
    } */

    List<Widget> createListCorta() {
      return items.map((e) {
        return Column(
          children: <Widget>[
            ListTile(
              leading: const Icon(Icons.touch_app_rounded),
              title: Text('$e!'),
              subtitle: const Text('Cualquier cosa'),
              trailing: const Icon(Icons.arrow_forward_ios_rounded),
              onTap: () {},
            ),
            const Divider()
          ],
        );
      }).toList();
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Componentes temp'),
      ),
      body: ListView(
        children: createListCorta(),
      ),
    );
  }
}
