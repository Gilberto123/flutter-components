import 'package:flutter/material.dart';

class CardScreen extends StatelessWidget {
  const CardScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Cards Screen"),
        ),
        body: ListView(
          padding: const EdgeInsets.all(10.0),
          children: [
            cardType1(),
            const SizedBox(
              height: 30.0,
            ),
            cardType2(),
            const SizedBox(
              height: 30.0,
            ),
            cardType1(),
            const SizedBox(
              height: 30.0,
            ),
            cardType2(),
            const SizedBox(
              height: 30.0,
            ),
            cardType1(),
            const SizedBox(
              height: 30.0,
            ),
            cardType2(),
            const SizedBox(
              height: 30.0,
            ),
            cardType1(),
            const SizedBox(
              height: 30.0,
            ),
            cardType2(),
            const SizedBox(
              height: 30.0,
            )
          ],
        ));
  }

  Widget cardType1() {
    return Card(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        elevation: 10.0,
        child: Column(
          children: [
            const ListTile(
              leading: Icon(Icons.photo_album, color: Colors.blue),
              title: Text("Im a type One card"),
              subtitle: Text("Im a card type One description"),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                    onPressed: () {
                      print("Tap on Text button");
                    },
                    child: const Text("Aceptar")),
                IconButton(
                    onPressed: () {
                      print("Tap on Icon button");
                    },
                    icon: const Icon(Icons.close))
              ],
            )
          ],
        ));
  }

  Widget cardType2() {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                spreadRadius: 2.0,
                offset: Offset(2.0, 10.0))
          ],
          borderRadius: BorderRadius.circular(30.0)),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: const Column(
          children: [
            FadeInImage(
                height: 157,
                fit: BoxFit.contain,
                fadeInDuration: Duration(milliseconds: 200),
                placeholder: AssetImage("assets/jar-loading.gif"),
                image: NetworkImage(
                    "https://i.pinimg.com/originals/db/f4/af/dbf4af88edfe3d1b13c517aa0a933305.jpg")),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [Text("Image description")],
            )
          ],
        ),
      ),
    );
  }
}
