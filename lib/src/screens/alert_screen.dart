import 'package:flutter/material.dart';

class AlertScreen extends StatelessWidget {
  const AlertScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Alert Screen')),
      body: Center(
        child: TextButton(
          style: const ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(Colors.blue),
              foregroundColor: MaterialStatePropertyAll(Colors.white),
              textStyle: MaterialStatePropertyAll(
                  TextStyle(fontWeight: FontWeight.bold))),
          onPressed: () => _showAlert(context),
          child: const Text("Show Alert"),
        ),
      ),
    );
  }

  Future<void> _showAlert(BuildContext context) {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text("Alert"),
            content: const Column(
                mainAxisSize: MainAxisSize.min,
                children: [Text("Alert Content"), FlutterLogo(size: 100)]),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text("Ok"),
              ),
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text("Cancel"),
              )
            ],
          );
        });
  }
}
