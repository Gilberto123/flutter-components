import 'package:flutter/material.dart';
import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/utils.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    // El Scaffold es la vista de la app
    return Scaffold(
      appBar: AppBar(
        title: const Text('Componentes'),
      ),
      body: lista(),
    );
  }

  Widget lista() {
    // Nospermite renderizar en pantalla en un stateless widget
    return FutureBuilder(
        // La funcion que retorna el Future
        future: menuProvider.cargarData(),
        // La data inicial
        initialData: const [],
        // Lo que vas a renderizar
        builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
          return ListView(
            children: createListItems(snapshot.requireData, context),
          );
        });
  }

  // Funcion que crea los items de la lista en base a la data
  List<Widget> createListItems(List<dynamic> data, BuildContext context) {
    // Items vacios
    List<Widget> opts = [];

    // Recorrido de la data
    for (var opt in data) {
      // Agregando cada item con la data
      opts
        ..add(ListTile(
          title: Text(opt['texto']),
          leading: getIcon(opt['icon']),
          trailing: const Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.pushNamed(context, opt['ruta']);
          },
        ))
        ..add(const Divider());
    }

    // Retornando los items
    return opts;
  }
}
