import 'package:flutter/material.dart';

class InputScreen extends StatefulWidget {
  const InputScreen({super.key});

  @override
  State<InputScreen> createState() => _InputScreenState();
}

class _InputScreenState extends State<InputScreen> {
  // Variable que almacena el valor del nombre
  String _name = "";
  // Variable que almacena el valor del email
  String _email = "";
  // Variable que almacena el valor del password
  String _passwd = "";
  // Variable que almacena el valor de la fecha de nacimiento
  String _date = "";
  // Opción seleccionada
  String _dropdownSelected = "Volar";

  // Controler del campo de la fecha de nacimiento
  final TextEditingController _inputFieldDateController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Input Screen"),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
        children: [
          _createInput(),
          const Divider(),
          _createEmail(),
          const Divider(),
          _createPass(),
          const Divider(),
          _createDate(context),
          const Divider(),
          _createDropDown(),
          const Divider(),
          _createPerson(),
        ],
      ),
    );
  }

  Widget _createInput() {
    return TextField(
      autofocus: true,
      textCapitalization: TextCapitalization.words,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          counter: Text("Letras ${_name.length}"),
          hintText: "Nombre",
          labelText: "Nombre Completo",
          helperText: "Sin apellidos",
          suffixIcon: const Icon(Icons.accessibility),
          icon: const Icon(Icons.account_circle)),
      onChanged: (value) {
        setState(() {
          _name = value;
        });
        print(_name);
      },
    );
  }

  Widget _createPerson() {
    return ListTile(
      title: Text("Nombre: $_name"),
      subtitle: Text("Email: $_email"),
      leading: Icon(_passwd == "" ? Icons.lock_open : Icons.lock),
      trailing: Text(_dropdownSelected),
    );
  }

  Widget _createEmail() {
    return TextField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          hintText: "email",
          labelText: "email",
          suffixIcon: const Icon(Icons.alternate_email),
          icon: const Icon(Icons.email)),
      onChanged: (value) {
        setState(() {
          _email = value;
        });
        print(_email);
      },
    );
  }

  Widget _createPass() {
    return TextField(
      obscureText: true,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          hintText: "Password",
          labelText: "Password",
          suffixIcon: const Icon(Icons.lock_open),
          icon: const Icon(Icons.lock)),
      onChanged: (value) {
        setState(() {
          _passwd = value;
        });
        print(_passwd);
      },
    );
  }

  Widget _createDate(BuildContext context) {
    return TextField(
      enableInteractiveSelection: false,
      controller: _inputFieldDateController,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          hintText: "Fecha de nacimiento",
          labelText: "Fecha de nacimiento",
          suffixIcon: const Icon(Icons.perm_contact_cal),
          icon: const Icon(Icons.calendar_today)),
      onTap: () {
        // Se quita el foco del input para poder mostrar el modal
        FocusScope.of(context).requestFocus(FocusNode());
        // Mostrar modal, se manda el context por que se requiere construir en pantalla de forma dinamica
        _selectDate(context);
      },
    );
  }

  void _selectDate(BuildContext context) async {
    // Mostrando datePicker
    DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime(2025),
        locale: const Locale("es"));

    // Si se selecciona alguna fecha
    // Se settea el estado del widget
    setState(() {
      // Se obtiene solo la fecha, excluyendo horas
      _date = picked.toString().split(" ")[0];
      // Se renderiza la fecha obtenida en el input de la fecha
      _inputFieldDateController.text = _date;
    });
  }

  final List<String> _powers = ['Volar', 'Rayos X', 'Regeneración', 'Super fuerza'];

  List<DropdownMenuItem<String>> getDropdownOptions(){
    List<DropdownMenuItem<String>> lista = [];

    _powers.forEach(
      (power) {
        lista.add(
          DropdownMenuItem(value: power, child: Text(power))
        );
       }
    );

    return lista;
  }
    
  Widget _createDropDown() {

    return Row(
      children: [
        const Icon(Icons.select_all),
        const SizedBox(width: 30.0,),
        DropdownButton(
          items: getDropdownOptions(),
          value: _dropdownSelected,
          onChanged: (opt){
            setState(() {
              _dropdownSelected = opt.toString();
            });
          }
        )
      ],

    );    
  }
}
