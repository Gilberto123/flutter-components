import 'package:componentes/src/screens/animated_container.dart';
import 'package:componentes/src/screens/input_screen.dart';
import 'package:componentes/src/screens/listview_screen.dart';
import 'package:componentes/src/screens/slider_screen.dart';
import 'package:flutter/material.dart';

import 'package:componentes/src/screens/home_screen.dart';
import 'package:componentes/src/screens/alert_screen.dart';
import 'package:componentes/src/screens/avantar_screen.dart';
import 'package:componentes/src/screens/card_screen.dart';

Map<String, WidgetBuilder> getAppRoutes() {
  return <String, WidgetBuilder>{
    '/': (BuildContext context) => const HomeScreen(),
    'alert': (context) => const AlertScreen(),
    'avatar': (context) => const AvatarScreen(),
    'card': (context) => const CardScreen(),
    "animatedContainer": (context) => const AnimatedContainerScreen(),
    "inputs": (context) => const InputScreen(),
    "sliders": (context) => const SliderScreen(),
    "lists": (context) => const LstScreen()
  };
}
