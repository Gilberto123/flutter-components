import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

// Simulacion de un service
class _MenuProvider {
  // Lista que almacena la data que viene desde la API
  List<dynamic> opciones = [];

  // Constructor de la clase
  _MenuProvider() {
    // cargarData();
  }

  // El future es como una promesa
  Future<List<dynamic>> cargarData() async {
    // Hace la petición de la data
    final response = await rootBundle.loadString('data/menu_opts.json');

    // Mapea la data
    Map dataMap = json.decode(response);
    opciones = dataMap['rutas'];

    // Retorna el future con la data
    return opciones;
  }
}

// Instancia la clase service
final menuProvider = _MenuProvider();
